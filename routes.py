from flask import Flask

app = Flask(__name__)

@app.route("/helloworld", methods=["GET"])
def helloWorld():
    return {"hello": "world"}

@app.route("/cadastra/usuario", methods=["POST"])
def cadastraUsuario():
    return {"id": 0}

if __name__ == '__main__':
   app.run(debug=False,host='0.0.0.0', port=5002)